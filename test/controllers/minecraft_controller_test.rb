require 'test_helper'

class MinecraftControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get auth" do
    get :auth
    assert_response :success
  end

  test "should get stop" do
    get :stop
    assert_response :success
  end

  test "should get start" do
    get :start
    assert_response :success
  end

end
